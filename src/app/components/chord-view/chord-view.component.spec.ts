import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChordViewComponent } from './chord-view.component';

describe('ChordViewComponent', () => {
  let component: ChordViewComponent;
  let fixture: ComponentFixture<ChordViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChordViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChordViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
